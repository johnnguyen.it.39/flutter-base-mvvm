import 'package:base_mvvm/common/mixin/loading_mixin.dart';
import 'package:base_mvvm/network/firebase/firebase_request_handler.dart';
import 'package:base_mvvm/network/firebase/user_detail.dart';
import 'package:base_mvvm/service_locator.dart';
import 'package:flutter/cupertino.dart';

import 'package:rxdart/rxdart.dart';

class PartnerProfileViewModel extends ChangeNotifier
    with FirebaseRequestHandler, LoadingsMixin {
  final userData = BehaviorSubject<UserDetail>();

  void init(int? targetId) async {
    if (targetId != null) {
      getPartnerDetail(targetId).then((value) {
        if (value != null) {
          userData.add(value);
        }
      });
    }
  }
}
