import 'package:base_mvvm/common/assets/app_assets.dart';
import 'package:base_mvvm/common/assets/app_colors.dart';
import 'package:base_mvvm/common/ui/base_view.dart';
import 'package:base_mvvm/common/widgets/c_button.dart';
import 'package:base_mvvm/common/widgets/c_text.dart';
import 'package:base_mvvm/extensions/int_extension.dart';
import 'package:base_mvvm/network/firebase/user_detail.dart';
import 'package:base_mvvm/pages/partner_profile_detail/view_model/partner_profile_view_model.dart';
import 'package:flutter/material.dart';

class PartnerProfileDetailPage extends StatefulWidget {
  final int? targetId;
  const PartnerProfileDetailPage({Key? key, this.targetId}) : super(key: key);

  @override
  State<PartnerProfileDetailPage> createState() =>
      _PartnerProfileDetailPageState();
}

class _PartnerProfileDetailPageState extends State<PartnerProfileDetailPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<PartnerProfileViewModel>(
      onModelReady: (viewModel) {
        viewModel.init(widget.targetId);
      },
      builder: (context, viewModel, child) {
        return Scaffold(
          //resizeToAvoidBottomInset: false,
          backgroundColor: Colors.white,
          body: StreamBuilder<UserDetail?>(
              stream: viewModel.userData,
              builder: (context, snapshot) {
                return SafeArea(
                  child: Stack(
                    children: [
                      Stack(
                        children: [
                          snapshot.data?.avatar != null
                              ? Image.network(
                                  snapshot.data?.avatar ?? '',
                                  fit: BoxFit.cover,
                                  width: double.infinity,
                                  height: 500,
                                )
                              : Container(),
                          Container(
                            height: 500,
                            color: Colors.black.withOpacity(0.4),
                          ),
                        ],
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: SingleChildScrollView(
                          child: Container(
                            width: double.infinity,
                            height: 450,
                            decoration: const BoxDecoration(
                              shape: BoxShape.rectangle,
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(80.0),
                                topRight: Radius.circular(80.0),
                              ),
                            ),
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 16),
                              child: SingleChildScrollView(
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: [
                                    if (snapshot.hasData) ...[
                                      32.height,
                                      Row(
                                        children: [
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              CText(
                                                padding: const EdgeInsets.only(
                                                    left: 14),
                                                text: snapshot.data?.name ?? '',
                                                fontSize: 18,
                                                textColor:
                                                    AppColors.tinderColor,
                                                fontWeight: FontWeight.bold,
                                              ),
                                              12.height,
                                              Row(
                                                children: [
                                                  CText(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 14),
                                                    text:
                                                        '${snapshot.data?.age.toString()}, ',
                                                    fontSize: 14,
                                                    textColor: Colors.grey,
                                                  ),
                                                  CText(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 4),
                                                    text:
                                                        snapshot.data?.gender ==
                                                                1
                                                            ? 'Male'
                                                            : 'Female',
                                                    fontSize: 14,
                                                    textColor: Colors.grey,
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                          const Spacer(),
                                          const CButton(
                                            radius: 20,
                                            backgroundColor:
                                                AppColors.mainColors,
                                            width: 100,
                                            height: 32,
                                            title: '2.5 miles',
                                            titleColor: Colors.white,
                                            titleFontSize: 12,
                                          ),
                                          8.width,
                                          const Icon(
                                            Icons.send_outlined,
                                            color:
                                                AppColors.descriptionTextColor,
                                            size: 24,
                                          ),
                                          8.width,
                                        ],
                                      ),
                                      12.height,
                                      CText(
                                        padding:
                                            const EdgeInsets.only(left: 14),
                                        text: snapshot.data?.description ?? '',
                                        fontSize: 12,
                                        lineSpacing: 1.5,
                                      ),
                                      12.height,
                                      const CText(
                                        padding: EdgeInsets.only(left: 14),
                                        text: 'INTERESTS:',
                                        fontSize: 16,
                                        textColor: AppColors.mainOrange,
                                        fontWeight: FontWeight.bold,
                                      ),
                                      8.height,
                                      SizedBox(
                                        height: 16,
                                        child: ListView.builder(
                                          scrollDirection: Axis.horizontal,
                                          itemCount:
                                              snapshot.data?.interest?.length,
                                          itemBuilder: (context, i) {
                                            return CText(
                                              padding: const EdgeInsets.only(
                                                  left: 14),
                                              text:
                                                  '${snapshot.data?.interest?[i]}',
                                              fontSize: 12,
                                              textColor: Colors.grey,
                                            );
                                          },
                                        ),
                                      ),
                                      8.height,
                                      additionalInformationItem(
                                          title: 'LOOKING FOR',
                                          data: snapshot.data?.datingPurpose),
                                      additionalInformationItem(
                                          title: 'WANT TO DATE/MEET ?',
                                          data: snapshot.data?.genderPrefer == 1
                                              ? 'MALE'
                                              : 'FEMALE'),
                                      additionalInformationItem(
                                          title: 'HEIGHT',
                                          data: snapshot.data?.height),
                                      additionalInformationItem(
                                          title: 'HAIR COLOR',
                                          data: snapshot.data?.hairColor),
                                      additionalInformationItem(
                                          title: 'RELIGION',
                                          data: snapshot.data?.religion),
                                      additionalInformationItem(
                                          title: 'EDUCATION',
                                          data: snapshot.data?.education),
                                      additionalInformationItem(
                                          title: 'OCCUPATION',
                                          data: snapshot.data?.occupation),
                                    ]
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.topLeft,
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 8, top: 8),
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: const Icon(
                                  Icons.arrow_back,
                                  color: Colors.white,
                                  size: 32,
                                ),
                              ),
                            ),
                            const Spacer(),
                            Padding(
                              padding: const EdgeInsets.only(top: 8, right: 8),
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: const Icon(
                                  Icons.more_horiz_outlined,
                                  color: Colors.white,
                                  size: 32,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                );
              }),
        );
      },
    );
  }

  Widget additionalInformationItem({String? title, String? data}) {
    return SizedBox(
      height: 32,
      child: Row(
        children: [
          CText(
            padding: const EdgeInsets.only(left: 14),
            text: title,
            fontSize: 12,
            textColor: AppColors.mainColors,
            fontWeight: FontWeight.bold,
          ),
          const Spacer(),
          CText(
            padding: const EdgeInsets.only(left: 14),
            text: data,
            fontSize: 12,
          )
        ],
      ),
    );
  }
}
