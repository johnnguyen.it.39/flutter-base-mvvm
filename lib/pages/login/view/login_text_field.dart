import 'package:base_mvvm/extensions/int_extension.dart';
import 'package:flutter/material.dart';

class LoginTextField extends StatelessWidget {
  final Icon? prefixIcon;
  final String? hint;
  final String? label;
  final bool? isSecure;
  final TextEditingController? controller;

  const LoginTextField(
      {Key? key,
      this.prefixIcon,
      this.hint,
      this.label,
      this.isSecure,
      this.controller})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 44,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(
            height: 40,
            child: Row(
              children: [
                SizedBox(
                  child: prefixIcon,
                ),
                12.width,
                Expanded(
                  child: TextField(
                    controller: controller,
                    obscureText: isSecure ?? false,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      labelText: label,
                      hintText: hint,
                    ),
                  ),
                ),
              ],
            ),
          ),
          2.height,
          const Divider(
            height: 1,
            color: Colors.grey,
          )
        ],
      ),
    );
  }
}
