import 'package:base_mvvm/common/assets/app_assets.dart';
import 'package:base_mvvm/common/assets/app_colors.dart';
import 'package:base_mvvm/common/ui/base_view.dart';
import 'package:base_mvvm/common/widgets/c_button.dart';
import 'package:base_mvvm/common/widgets/c_text.dart';
import 'package:base_mvvm/extensions/int_extension.dart';
import 'package:base_mvvm/pages/login/view_model/login_view_model.dart';
import 'package:flutter/material.dart';
import 'login_text_field.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return BaseView<LoginViewModel>(
        onModelReady: (viewModel) {},
        builder: (context, viewModel, child) {
          return Scaffold(
            //resizeToAvoidBottomInset: false,
            backgroundColor: Colors.white,
            body: SafeArea(
              child: Stack(
                children: [
                  Stack(
                    children: [
                      Image.asset(
                        AppAssets.loginImage,
                        fit: BoxFit.fill,
                        width: double.infinity,
                        height: 500,
                      ),
                      Container(
                        height: 500,
                        color: Colors.black.withOpacity(0.4),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 140),
                          child: Column(
                            children: [
                              Image.asset(
                                AppAssets.tinderLogo,
                                fit: BoxFit.fill,
                                height: 88,
                              ),
                              12.height,
                              const CText(
                                fontSize: 24,
                                text: 'Hello',
                                textColor: Colors.white,
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      height: 450,
                      decoration: const BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(80.0),
                          topRight: Radius.circular(80.0),
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 24),
                        child: SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              56.height,
                              const CText(
                                padding: EdgeInsets.only(left: 14),
                                text: 'Login',
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                              16.height,
                              LoginTextField(
                                prefixIcon: const Icon(
                                  Icons.person,
                                  color: AppColors.mainOrange,
                                  size: 24.0,
                                ),
                                hint: 'Email',
                                controller: viewModel.emailTextController,
                              ),
                              16.height,
                              LoginTextField(
                                prefixIcon: const Icon(
                                  Icons.lock,
                                  color: AppColors.mainOrange,
                                  size: 24.0,
                                ),
                                controller: viewModel.passwordTextController,
                                hint: 'Password',
                                isSecure: true,
                              ),
                              24.height,
                              CButton(
                                width: double.infinity,
                                height: 40,
                                backgroundColor: AppColors.tinderColor,
                                title: 'Login',
                                titleColor: Colors.white,
                                radius: 10,
                                buttonTapped: () {
                                  viewModel.login(context);
                                },
                              ),
                              12.height,
                              Row(
                                children: const [
                                  CText(
                                    text: 'Forgot password ?',
                                    textColor: AppColors.mainOrange,
                                    textDecoration: TextDecoration.underline,
                                  ),
                                  Spacer(),
                                  CText(
                                    fontWeight: FontWeight.bold,
                                    text: 'Sign up',
                                    textColor: AppColors.mainOrange,
                                    textDecoration: TextDecoration.underline,
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
