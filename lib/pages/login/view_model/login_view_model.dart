import 'package:base_mvvm/common/mixin/loading_mixin.dart';
import 'package:base_mvvm/network/http_requests/repositories/auth_repository.dart';
import 'package:base_mvvm/router/app_routes.dart';
import 'package:flutter/cupertino.dart';

class LoginViewModel extends ChangeNotifier with LoadingsMixin {
  final authRepository = AuthRepository();

  TextEditingController emailTextController = TextEditingController();
  TextEditingController passwordTextController = TextEditingController();

  LoginViewModel() {
    emailTextController.text = "johnnguyen.it.39@gmail.com";
    passwordTextController.text = "flutter.dev";
  }

  login(BuildContext context) async {
    //Note: Loading and stop loading can be wrap in to general component don't need to be call for each function this is just for testing.
    showLoading();
    final response = await authRepository.login(
      emailTextController.text,
      passwordTextController.text,
    );
    dissmissLoading();
    if (response.status == 1) {
      Navigator.pushNamed(context, AppRoutes.HOME);
    }
  }
}
