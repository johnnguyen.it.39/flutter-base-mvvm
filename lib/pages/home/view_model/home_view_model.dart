import 'package:base_mvvm/network/firebase/firebase_request_handler.dart';
import 'package:base_mvvm/network/firebase/user_detail.dart';
import 'package:flutter/widgets.dart';
import 'package:rxdart/rxdart.dart';

class HomeViewModel extends ChangeNotifier with FirebaseRequestHandler {
  final listUsers = BehaviorSubject<List<UserDetail?>>();
  final interactionSubject = PublishSubject<InteractionModel>();
  List<int?>? favoriteList = [];

  initData() async {
    favoriteList = await getFavoriteList();
    List<UserDetail?> tempList = [];
    final matchListResponse = await getMatchList();
    for (var element in matchListResponse) {
      tempList.add(element);
    }
    listUsers.add(tempList);
  }

  bool? isLikeUser(int? partnerId) {
    if (favoriteList != null && favoriteList!.contains(partnerId)) {
      return true;
    }
    return false;
  }

  matchUser(int? id) {
    interactionSubject.add(InteractionModel(
      id: id,
      islike: true,
    ));
    confirmMatch(id);
  }

  disLikePartner(int? id) async {
    await dislike(id);
  }
}

class InteractionModel {
  int? id;
  bool? islike;

  InteractionModel({this.id, this.islike});
}
