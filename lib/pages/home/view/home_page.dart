import 'package:base_mvvm/common/assets/app_colors.dart';
import 'package:base_mvvm/common/ui/base_view.dart';
import 'package:base_mvvm/common/widgets/c_container.dart';
import 'package:base_mvvm/common/widgets/c_text.dart';
import 'package:base_mvvm/extensions/int_extension.dart';
import 'package:base_mvvm/network/firebase/user_detail.dart';
import 'package:base_mvvm/pages/home/view_model/home_view_model.dart';
import 'package:base_mvvm/router/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:tcard/tcard.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final TCardController _controller = TCardController();

  @override
  Widget build(BuildContext context) {
    return BaseView<HomeViewModel>(
      onModelReady: (viewModel) {
        viewModel.initData();
      },
      builder: (context, viewModel, child) {
        return Scaffold(
          body: SafeArea(
              child: Column(
            children: [
              //Want have global view-model
              _appBar(),
              swipeWidget(viewModel),
              _bottomBar(),
            ],
          )),
        );
      },
    );
  }

  Widget _appBar() {
    return Container(
      height: 48,
      color: AppColors.tinderColor,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12),
        child: Row(
          children: [
            const CText(
              textColor: Colors.white,
              text: 'Recommended',
              fontWeight: FontWeight.bold,
              fontSize: 14,
            ),
            const Spacer(),
            const SizedBox(
              child: Icon(
                Icons.search,
                color: Colors.white,
                size: 24.0,
              ),
            ),
            8.width,
            const SizedBox(
              child: Icon(
                Icons.filter_list_sharp,
                color: Colors.white,
                size: 24.0,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _bottomBar() {
    return StreamBuilder<Object>(
        stream: null,
        builder: (context, snapshot) {
          return Container(
            color: Colors.white,
            height: 48,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: const [
                SizedBox(
                  child: Icon(
                    Icons.home,
                    color: AppColors.mainOrange,
                    size: 24.0,
                  ),
                ),
                SizedBox(
                  child: Icon(
                    Icons.location_pin,
                    color: Colors.grey,
                    size: 24.0,
                  ),
                ),
                SizedBox(
                  child: Icon(
                    Icons.message,
                    color: Colors.grey,
                    size: 24.0,
                  ),
                ),
                SizedBox(
                  child: Icon(
                    Icons.notification_important,
                    color: Colors.grey,
                    size: 24.0,
                  ),
                ),
                SizedBox(
                  child: Icon(
                    Icons.person,
                    color: Colors.grey,
                    size: 24.0,
                  ),
                ),
              ],
            ),
          );
        });
  }

  Widget swipeWidget(HomeViewModel viewModel) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: StreamBuilder<List<UserDetail?>>(
            stream: viewModel.listUsers,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return TCard(
                  controller: _controller,
                  cards: generateSwipeCards(snapshot.data!, viewModel),
                  onForward: (index, _) {
                    print('Forward');
                  },
                );
              } else {
                return Container();
              }
            }),
      ),
    );
  }

  List<Widget> generateSwipeCards(
      List<UserDetail?> users, HomeViewModel viewModel) {
    return List.generate(
      users.length,
      (int index) {
        return Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(16.0),
            boxShadow: const [
              BoxShadow(
                offset: Offset(0, 17),
                blurRadius: 23.0,
                spreadRadius: -13.0,
                color: Colors.black54,
              )
            ],
          ),
          child: Column(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Map<String, dynamic> args = {
                      'partner_id': users[index]?.id,
                    };
                    Navigator.pushNamed(
                      context,
                      AppRoutes.PARTNER_PROFILE_DETAIL,
                      arguments: args,
                    );
                  },
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(16.0),
                      child: Image.network(
                        '${users[index]?.avatar}',
                        fit: BoxFit.cover,
                      )),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 12),
                child: SizedBox(
                  height: 80,
                  child: StreamBuilder<InteractionModel>(
                      stream: viewModel.interactionSubject.stream,
                      builder: (context, snapshot) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            CContainer(
                              tappedContainer: () async {
                                await viewModel
                                    .disLikePartner(users[index]?.id);
                                _controller.forward();
                              },
                              child: const Icon(
                                Icons.cancel_sharp,
                                color: Colors.grey,
                                size: 40.0,
                              ),
                            ),
                            if (snapshot.data?.islike == true &&
                                    users[index]?.id == snapshot.data?.id ||
                                viewModel.isLikeUser(users[index]?.id) ==
                                    true) ...[
                              CContainer(
                                tappedContainer: () {
                                  Map<String, dynamic> args = {
                                    'partner_id': users[index]?.id,
                                  };
                                  Navigator.pushNamed(
                                    context,
                                    AppRoutes.MATCH,
                                    arguments: args,
                                  );
                                },
                                child: const Icon(
                                  Icons.message,
                                  color: AppColors.tinderColor,
                                  size: 56.0,
                                ),
                              ),
                            ],
                            CContainer(
                              tappedContainer: () {
                                viewModel.matchUser(users[index]?.id);
                              },
                              child: const Icon(
                                Icons.favorite,
                                color: Colors.green,
                                size: 40.0,
                              ),
                            ),
                          ],
                        );
                      }),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
