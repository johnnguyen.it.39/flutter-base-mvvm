import 'package:base_mvvm/common/assets/app_assets.dart';
import 'package:base_mvvm/common/assets/app_colors.dart';
import 'package:base_mvvm/common/ui/base_view.dart';
import 'package:base_mvvm/common/widgets/c_button.dart';
import 'package:base_mvvm/common/widgets/c_text.dart';
import 'package:base_mvvm/extensions/int_extension.dart';
import 'package:base_mvvm/pages/match/view_model/match_view_model.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

class MatchPage extends StatefulWidget {
  final int? partnerId;
  const MatchPage({Key? key, this.partnerId}) : super(key: key);

  @override
  State<MatchPage> createState() => _MatchPageState();
}

class _MatchPageState extends State<MatchPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: BaseView<MatchViewModel>(
        onModelReady: (viewModel) {
          viewModel.intData(widget.partnerId);
        },
        builder: (context, viewModel, child) {
          return Column(
            children: [
              120.height,
              const CText(
                text: "It's a Match!",
                fontSize: 24,
                textColor: AppColors.tinderColor,
                fontWeight: FontWeight.bold,
              ),
              48.height,
              SizedBox(
                height: 240,
                child: Stack(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Transform.rotate(
                          angle: -math.pi / 16,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10.0),
                            child: Image.asset(
                              AppAssets.profileImage,
                              width: 120,
                              height: 200,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Transform.rotate(
                          angle: math.pi / 16,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10.0),
                            child: StreamBuilder<String?>(
                                stream: viewModel.partnerProfilePictureSubject,
                                builder: (context, snapshot) {
                                  if (snapshot.hasData) {
                                    return Image.network(
                                      snapshot.data!,
                                      width: 120,
                                      height: 200,
                                      fit: BoxFit.cover,
                                    );
                                  } else {
                                    return Container();
                                  }
                                }),
                          ),
                        ),
                      ],
                    ),
                    const Center(
                      child: Icon(
                        Icons.favorite,
                        color: AppColors.mainColors,
                        size: 56.0,
                      ),
                    )
                  ],
                ),
              ),
              12.height,
              const CText(
                textAlign: TextAlign.center,
                width: 300,
                text: "Now you can start a conversation",
                fontWeight: FontWeight.bold,
                textColor: AppColors.mainColors,
              ),
              16.height,
              const CText(
                textColor: AppColors.textGrey,
                textAlign: TextAlign.center,
                text: "maybe? with a simple wave",
              ),
              const Spacer(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const CButton(
                    radius: 10,
                    height: 40,
                    title: 'Say Hi',
                    titleColor: Colors.white,
                    width: 120,
                    backgroundColor: AppColors.tinderColor,
                  ),
                  16.width,
                  CButton(
                    buttonTapped: () {
                      Navigator.pop(context);
                    },
                    radius: 10,
                    height: 40,
                    title: 'Keep Swiping',
                    titleColor: Colors.white,
                    width: 120,
                    backgroundColor: AppColors.mainColors,
                  ),
                ],
              ),
              88.height,
            ],
          );
        },
      )),
    );
  }
}
