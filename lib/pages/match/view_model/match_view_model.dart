import 'package:base_mvvm/network/firebase/firebase_request_handler.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class MatchViewModel extends ChangeNotifier with FirebaseRequestHandler {
  final partnerProfilePictureSubject = PublishSubject<String?>();
  intData(int? id) async {
    final user = await getPartnerDetail(id);

    if (user != null) {
      partnerProfilePictureSubject.add(user.avatar);
    }
  }
}
