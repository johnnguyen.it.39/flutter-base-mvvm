import 'package:base_mvvm/network/firebase/user_detail.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

mixin FirebaseRequestHandler {
  //NOTICE: Assume the current user has the id: 0

  //Improve later
  final userDetail = FirebaseFirestore.instance
      .collection('users')
      .doc('bRrceSFwblWGE4ktpx8k')
      .collection('user_detail');

  Future<List<UserDetail?>> getMatchList() async {
    List<UserDetail?> matchList = [];
    final userDetailSnapshot = await userDetail.get();
    for (var element in userDetailSnapshot.docs) {
      var item = UserDetail(snapshot: element.data());
      if (item.id == 0) {
        item.matchList?.forEach((element) async {
          final user = await getPartnerDetail(element);
          matchList.add(user);
        });
        break;
      }
    }
    final dislikeList = await _getDisLikeList();
    dislikeList?.forEach((dislikeId) {
      matchList.removeWhere((item) => item?.id == dislikeId);
    });

    return matchList;
  }

  Future<List<int>?> _getDisLikeList() async {
    List<int> dislikeList = [];
    final userDetailSnapshot = await userDetail.get();
    for (var element in userDetailSnapshot.docs) {
      var item = UserDetail(snapshot: element.data());
      if (item.id == 0) {
        item.dislikeList?.forEach((element) {
          dislikeList.add(element as int);
        });
        return dislikeList;
      }
    }
    return dislikeList;
  }

  Future<List<int?>?> getFavoriteList() async {
    List<int> favoriteList = [];
    final userDetailSnapshot = await userDetail.get();
    for (var element in userDetailSnapshot.docs) {
      var item = UserDetail(snapshot: element.data());
      if (item.id == 0) {
        item.favoriteList?.forEach((element) {
          favoriteList.add(element as int);
        });
        return favoriteList;
      }
    }
    return favoriteList;
  }

  Future<bool> isLikedPartner(int? partnerId) async {
    final favoritelist = await getFavoriteList();
    if (favoritelist != null && favoritelist.contains(partnerId)) {
      return true;
    }
    return false;
  }

  dislike(int? partnerId) {
    if (partnerId == null) {
      return;
    }
    userDetail.snapshots().listen((userDetailSnapshot) async {
      for (var element in userDetailSnapshot.docs) {
        var item = UserDetail(snapshot: element.data());
        if (item.id == 0) {
          final autoKey = element.id;
          var dislikelist = await _getDisLikeList();
          dislikelist?.add(partnerId);
          dislikelist = dislikelist?.toSet().toList();
          userDetail.doc(autoKey).update({'dislike_list': dislikelist});
        }
      }
    });
  }

  confirmMatch(int? partnerId) {
    if (partnerId == null) {
      return;
    }
    userDetail.snapshots().listen((userDetailSnapshot) async {
      for (var element in userDetailSnapshot.docs) {
        var item = UserDetail(snapshot: element.data());
        if (item.id == 0) {
          final autoKey = element.id;
          var favoriteList = await getFavoriteList();
          favoriteList?.add(partnerId);
          favoriteList = favoriteList?.toSet().toList();
          userDetail.doc(autoKey).update({'favorite_list': favoriteList});
        }
      }
    });
  }

  int index = 10;

  Future<void> fakeData() async {
    for (index; index < 30; index++) {
      await userDetail.add({
        "id": index + 1,
        "name": 'Lisa Rey ${index + 1}',
        "age": 27,
        "avatar":
            'https://firebasestorage.googleapis.com/v0/b/flutter-mvvm-b99a0.appspot.com/o/woman_5.jpeg?alt=media&token=300c5fc8-5e5a-4299-8002-bb9fbf238980',
        "dating_purpose": 'Relationship',
        "description":
            'A good listener, I love having a good talk to know each other’s side, Mountain over beach :)',
        "dislike_list": [],
        "education": 'Bachelor',
        "gender": 2,
        "gender_prefer": 1,
        "hair_color": 'Black',
        "height": "5'11 feet",
        "interest": ['Photography', 'Dancing'],
        "match_list": [],
        "occupation": 'Developer',
        "religion": 'Catholic',
        "favorite_list": [],
      });
    }
  }

  Future<UserDetail?> getPartnerDetail(int? partnerId) async {
    final userDetailSnapshot = await userDetail.get();
    for (var element in userDetailSnapshot.docs) {
      var item = UserDetail(snapshot: element.data());
      if (item.id != 0 && item.id == partnerId) {
        return item;
      }
    }
    return null;
  }
}
