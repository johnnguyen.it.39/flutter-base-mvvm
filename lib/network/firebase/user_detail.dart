class UserDetail {
  int? id;
  String? name;
  int? age;
  String? datingPurpose;
  String? avatar;
  String? description;
  List<dynamic>? dislikeList;
  String? education;
  int? gender;
  int? genderPrefer;
  String? hairColor;
  String? height;
  List<dynamic>? interest;
  List<dynamic>? matchList;
  List<dynamic>? favoriteList;
  String? occupation;
  String? religion;

  UserDetail({Map<String, dynamic>? snapshot}) {
    id = snapshot?['id'];
    name = snapshot?['name'];
    age = snapshot?['age'];
    avatar = snapshot?['avatar'];
    datingPurpose = snapshot?['dating_purpose'];
    description = snapshot?['description'];
    dislikeList = snapshot?['dislike_list'];
    education = snapshot?['education'];
    gender = snapshot?['gender'];
    genderPrefer = snapshot?['gender_prefer'];
    hairColor = snapshot?['hair_color'];
    height = snapshot?['height'];
    interest = snapshot?['interest'];
    matchList = snapshot?['match_list'];
    occupation = snapshot?['occupation'];
    religion = snapshot?['religion'];
    favoriteList = snapshot?['favorite_list'];
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "name": name,
      "age": age,
      "avatar": avatar,
      "dating_purpose": datingPurpose,
      "description": description,
      "dislike_list": dislikeList,
      "education": education,
      "gender": gender,
      "gender_prefer": genderPrefer,
      "hair_color": hairColor,
      "height": height,
      "interest": interest,
      "match_list": matchList,
      "occupation": occupation,
      "religion": religion,
      "favorite_list": favoriteList,
    };
  }
}
