class LoginResponse {
  final int? status;
  final LoginData? data;
  final String? message;

  LoginResponse({
    this.status,
    this.data,
    this.message,
  });

  LoginResponse.fromJson(Map<String, dynamic> json)
      : status = json['status'] as int?,
        data = (json['data'] as Map<String, dynamic>?) != null
            ? LoginData.fromJson(json['data'] as Map<String, dynamic>)
            : null,
        message = json['message'] as String?;

  Map<String, dynamic> toJson() =>
      {'status': status, 'data': data?.toJson(), 'message': message};
}

class LoginData {
  final int? id;
  final String? name;
  final int? age;

  LoginData({
    this.id,
    this.name,
    this.age,
  });

  LoginData.fromJson(Map<String, dynamic> json)
      : id = json['id'] as int?,
        name = json['name'] as String?,
        age = json['age'] as int?;

  Map<String, dynamic> toJson() => {'id': id, 'name': name, 'age': age};
}
