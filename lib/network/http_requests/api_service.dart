import 'package:base_mvvm/common/mixin/dialog_mixin.dart';
import 'package:base_mvvm/common/mixin/loading_mixin.dart';
import 'package:dio/dio.dart';
import 'base_request/base_request.dart';
import 'base_response/api_response.dart';

abstract class RequestHandler {
  Future<Response> handleHttpRequest(RequestModel request);
}

class ApiService with LoadingsMixin, DialogsMixin implements RequestHandler {
  final _dio = Dio();
  String _debugUrl = "https://dev.johnnguyenit39.com";
  String _releaseUrl = "https://prod.johnnguyenit39.com";

  Future<ApiResponse> request(RequestModel request) async {
    switch (request.method) {
      case RequestMethod.getMethod:
        return await _requestGet(request);
      case RequestMethod.postMethod:
        return await _requestPost(request);
      case RequestMethod.putMethod:
        return await _requestPut(request);
    }
  }

  Future<ApiResponse> _requestGet(RequestModel request) async {
    final response = await handleHttpRequest(request);
    return ApiResponse.fromJson(response.data);
  }

  Future<ApiResponse> _requestPost(RequestModel request) async {
    final response = await handleHttpRequest(request);
    return ApiResponse.fromJson(response.data);
  }

  Future<ApiResponse> _requestPut(RequestModel request) async {
    final response = await handleHttpRequest(request);
    return ApiResponse.fromJson(response.data);
  }

  @override
  Future<Response> handleHttpRequest(RequestModel request) async {
    request.route = _debugUrl + request.route;
    const accessToken = '';
    final headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $accessToken',
    };
    switch (request.method) {
      case RequestMethod.getMethod:
        return await _dio.get(
          request.route,
          options: Options(
            contentType: Headers.formUrlEncodedContentType,
            headers: headers,
          ),
        );
      case RequestMethod.postMethod:
        return await _dio.post(
          request.route,
          options: Options(
            contentType: Headers.formUrlEncodedContentType,
            headers: headers,
          ),
        );
      case RequestMethod.putMethod:
        return await _dio.put(
          request.route,
          options: Options(
            contentType: Headers.formUrlEncodedContentType,
            headers: headers,
          ),
        );
    }
  }

  initInterceptor() {
    _dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (request, handler) {},
        onError: (e, handler) async {},
      ),
    );
  }
}
