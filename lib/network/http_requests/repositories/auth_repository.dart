import 'dart:convert';

import 'package:base_mvvm/network/http_requests/api_service.dart';
import 'package:base_mvvm/network/http_requests/base_response/api_response.dart';
import 'package:base_mvvm/service_locator.dart';

abstract class AuthRepositoryInterface {
  Future<ApiResponse> login(String email, String password);
}

class AuthRepository implements AuthRepositoryInterface {
  final apiService = locator<ApiService>();

  @override
  Future<ApiResponse> login(String email, String password) async {
    Map<String, dynamic> body = {
      "email": email,
      "password": password,
    };
    String jsonBody = json.encode(body);

    //Uncomment this for production purpose
    //return apiService.request(LoginRequest(jsonBody));
    await Future.delayed(const Duration(milliseconds: 100));
    //Testing
    return ApiResponse(status: 1, data: null, message: 'ok');
  }
}
