import 'package:base_mvvm/network/http_requests/base_request/base_request.dart';

class LoginRequest extends RequestModel {
  LoginRequest(String params)
      : super(
          '/login',
          RequestMethod.postMethod,
          params,
        );
}
