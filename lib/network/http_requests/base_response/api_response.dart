class ApiResponse {
  final int? status;
  dynamic data;
  final String? message;

  ApiResponse({
    this.status,
    this.data,
    this.message,
  });

  ApiResponse.fromJson(Map<String, dynamic> json)
      : status = json['status'] as int?,
        data = json['data'] as dynamic,
        message = json['message'] as String?;

  Map<String, dynamic> toJson() => {
        'status': status,
        'data': data,
        'message': message,
      };
}
