import 'package:flutter/material.dart';
import 'dart:ui' as ui;

class ScreenSize {
  static double screenWidth() {
    return ui.window.physicalSize.width / ui.window.devicePixelRatio;
  }

  static double screenHeight() {
    return ui.window.physicalSize.height / ui.window.devicePixelRatio;
  }
}
