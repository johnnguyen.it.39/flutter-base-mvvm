// ignore_for_file: constant_identifier_names

class AppRoutes {
  static const INDEX = '/index';
  static const LOGIN = '/login';
  static const HOME = '/home';
  static const PARTNER_PROFILE_DETAIL = '/partner_profile_detail';
  static const MATCH = '/match';
}
