import 'package:base_mvvm/app_delegate.dart';
import 'package:base_mvvm/pages/home/view/home_page.dart';
import 'package:base_mvvm/pages/login/view/login_page.dart';
import 'package:base_mvvm/pages/match/view/match_page.dart';
import 'package:base_mvvm/pages/partner_profile_detail/view/partner_profile_page.dart';
import 'package:flutter/material.dart';
import 'app_routes.dart';

class AppPages {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    Map<String, dynamic> args = {};
    if (settings.arguments != null) {
      args = settings.arguments as Map<String, dynamic>;
    }
    switch (settings.name) {
      case AppRoutes.INDEX:
        return MaterialPageRoute(builder: (_) => AppDelegate());
      case AppRoutes.LOGIN:
        return MaterialPageRoute(builder: (_) => LoginPage());
      case AppRoutes.HOME:
        return MaterialPageRoute(builder: (_) => HomePage());
      case AppRoutes.MATCH:
        return MaterialPageRoute(
            builder: (_) => MatchPage(
                  partnerId: args['partner_id'],
                ));
      case AppRoutes.PARTNER_PROFILE_DETAIL:
        return MaterialPageRoute(
            builder: (_) => PartnerProfileDetailPage(
                  targetId: args['partner_id'],
                ));
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                      child: Text('No route defined for ${settings.name}')),
                ));
    }
  }
}
