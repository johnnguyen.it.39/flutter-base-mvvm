class AppAssets {
  static const loginImage = 'assets/images/login_image.png';
  static const tinderLogo = 'assets/images/logo.png';
  static const profileImage = 'assets/images/my_profile_picture.jpeg';
}
