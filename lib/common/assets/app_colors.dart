import 'package:flutter/material.dart';

class AppColors {
  static const mainColors = Color(0xFF158FB6);
  static const mainOrange = Color(0xFFd83f35);
  static const tinderColor = Color(0xFFf75252);

  static const inactiveTabbar = Color(0xFF434343);
  static const unSelectedColor = Color(0xFF474747);
  static const backgroundColor = Color(0xFF17182F);
  static const titleTextColor = Color(0xFF474747);
  static const descriptionTextColor = Color(0xFF7B7B7B);
  static const textColor = Colors.white;
  static const textGrey = Color(0xFF7B7B7B);
  static const testListTextColor = Color(0xFF6C6A6A);
}
