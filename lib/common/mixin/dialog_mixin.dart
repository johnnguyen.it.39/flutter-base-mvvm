import 'package:base_mvvm/common/assets/app_colors.dart';
import 'package:base_mvvm/common/ui/app_context.dart';
import 'package:base_mvvm/common/widgets/c_button.dart';
import 'package:base_mvvm/common/widgets/c_text.dart';
import 'package:base_mvvm/extensions/int_extension.dart';
import 'package:base_mvvm/utils/screen_size.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

mixin DialogsMixin {
  var context = AppContext.navigatorKey.currentContext!;
  void dissmissDialog() {
    Navigator.of(context, rootNavigator: true).pop('dialog');
  }

  showAlertDialog(String title, String message, String buttonTitle,
      VoidCallback? onPressed) {
    showGeneralDialog(
      barrierLabel: "Barrier",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: Duration(milliseconds: 300),
      context: context,
      pageBuilder: (_, __, ___) {
        return Align(
          alignment: Alignment.center,
          child: IntrinsicHeight(
            child: Container(
              width: ScreenSize.screenWidth() * 0.9,
              child: Column(
                children: [
                  Container(
                    height: ScreenSize.screenHeight() / 15,
                    child: Stack(
                      children: [
                        Align(
                          alignment: Alignment.center,
                          child: CText(
                            text: title,
                            textColor: AppColors.mainColors,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Positioned(
                          top: 2,
                          right: 2,
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(context, rootNavigator: true)
                                  .pop('dialog');
                            },
                            child: Icon(
                              Icons.close,
                              color: Colors.white,
                              size: ScreenSize.screenWidth() / 16,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: 0.5,
                    color: Colors.white,
                  ),
                  (ScreenSize.screenHeight() ~/ 45).height,
                  CText(
                    textAlign: TextAlign.center,
                    text: message,
                    lineSpacing: 1.2,
                    textColor: AppColors.mainColors,
                    fontSize: 13,
                  ),
                  (ScreenSize.screenHeight() ~/ 35).height,
                  CButton(
                    radius: 10,
                    height: 24,
                    width: 72,
                    backgroundColor: AppColors.mainColors,
                    title: 'ok'.tr().toUpperCase(),
                    titleFontSize: 12,
                    titleColor: Colors.white,
                    buttonTapped: onPressed ??
                        () {
                          Navigator.of(context, rootNavigator: true)
                              .pop('dialog');
                        },
                  ),
                  (ScreenSize.screenHeight() ~/ 35).height,
                ],
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
        );
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
  }

  showAlertOptionsDialog(String title, String message,
      VoidCallback? onPressedAgree, VoidCallback? onPressCancel) {
    showGeneralDialog(
      barrierLabel: "Barrier",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: Duration(milliseconds: 300),
      context: context,
      pageBuilder: (_, __, ___) {
        return Align(
          alignment: Alignment.center,
          child: IntrinsicHeight(
            child: Container(
              width: ScreenSize.screenWidth() * 0.9,
              child: Column(
                children: [
                  Container(
                    height: ScreenSize.screenHeight() / 15,
                    child: Stack(
                      children: [
                        Align(
                          alignment: Alignment.center,
                          child: CText(
                            text: title,
                            textColor: AppColors.mainColors,
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Positioned(
                          top: 2,
                          right: 2,
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(context, rootNavigator: true)
                                  .pop('dialog');
                            },
                            child: Icon(
                              Icons.close,
                              color: AppColors.mainColors,
                              size: ScreenSize.screenWidth() / 16,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: 0.5,
                    color: Colors.white,
                  ),
                  (ScreenSize.screenHeight() ~/ 45).height,
                  CText(
                    text: message,
                    textColor: AppColors.mainColors,
                    fontSize: 12,
                  ),
                  (ScreenSize.screenHeight() ~/ 35).height,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CButton(
                        radius: 10,
                        height: 24,
                        width: 72,
                        backgroundColor: AppColors.mainColors,
                        title: 'ok'.tr().toUpperCase(),
                        titleFontSize: 12,
                        titleColor: Colors.white,
                        buttonTapped: onPressedAgree,
                      ),
                      (ScreenSize.screenWidth() ~/ 24).width,
                      CButton(
                        radius: 10,
                        height: 24,
                        width: 72,
                        title: 'close'.tr().toUpperCase(),
                        titleFontSize: 12,
                        titleColor: Colors.grey,
                        buttonTapped: onPressCancel ??
                            () {
                              Navigator.of(context, rootNavigator: true)
                                  .pop('dialog');
                            },
                      ),
                    ],
                  ),
                  (ScreenSize.screenHeight() ~/ 35).height,
                ],
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
        );
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
  }
}
