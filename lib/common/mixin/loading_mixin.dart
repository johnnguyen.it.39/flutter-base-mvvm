import 'package:base_mvvm/common/ui/app_context.dart';
import 'package:base_mvvm/common/widgets/c_container.dart';
import 'package:flutter/material.dart';
import '../assets/app_colors.dart';

mixin LoadingsMixin {
  var context = AppContext.navigatorKey.currentContext!;
  //TODO: add on will pop
  showLoading() {
    showDialog(
      useSafeArea: false,
      barrierColor: Colors.white.withOpacity(0),
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () async {
            return false;
          },
          child: Container(
            color: Colors.black.withOpacity(0.3),
            child: const Center(
              child: CContainer(
                radius: 5,
                backgroundColor: Colors.white,
                width: 40,
                height: 40,
                child: Center(
                  child: SizedBox(
                    width: 20,
                    height: 20,
                    child: CircularProgressIndicator(
                      strokeWidth: 1,
                      valueColor: const AlwaysStoppedAnimation<Color>(
                          AppColors.tinderColor),
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  void dissmissLoading() {
    Navigator.of(context, rootNavigator: true).pop('dialog');
  }
}
