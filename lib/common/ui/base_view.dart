import 'package:base_mvvm/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:stacked/stacked.dart';

class BaseView<T extends ChangeNotifier> extends StatelessWidget {
  final Function(T model) onModelReady;
  final Widget Function(BuildContext context, T model, Widget? child) builder;
  const BaseView({Key? key, required this.onModelReady, required this.builder})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<T>.reactive(
      viewModelBuilder: () => locator<T>(),
      onModelReady: (viewModel) {
        SchedulerBinding.instance?.addPostFrameCallback((_) {
          onModelReady(viewModel);
        });
      },
      builder: builder,
      disposeViewModel: false,
      onDispose: (T) {},
    );
  }
}
