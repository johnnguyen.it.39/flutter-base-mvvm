import 'package:base_mvvm/network/http_requests/api_service.dart';
import 'package:base_mvvm/pages/home/view_model/home_view_model.dart';
import 'package:base_mvvm/pages/login/view_model/login_view_model.dart';
import 'package:base_mvvm/pages/match/view_model/match_view_model.dart';
import 'package:base_mvvm/pages/partner_profile_detail/view_model/partner_profile_view_model.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  //Api Service
  locator.registerLazySingleton<ApiService>(() => ApiService());

  //1. Login
  locator.registerFactory<LoginViewModel>(() => LoginViewModel());

  //2. Home
  //locator.registerFactory<HomeViewModel>(() => HomeViewModel());
  locator.registerSingleton<HomeViewModel>(HomeViewModel());

  //3. Partner Detail
  locator.registerFactory<PartnerProfileViewModel>(
      () => PartnerProfileViewModel());

  //4. Match
  locator.registerFactory<MatchViewModel>(() => MatchViewModel());
}
